from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
@app.route('/home')
@app.route('/home.html')
def hello():
    return render_template('home.html')
    
@app.route('/lb.html')
def lb():
     return render_template('lb.html')

if __name__ == '__main__':
    app.run(port=8080, host='0.0.0.0', debug=True)
    