DROP TABLE IF EXISTS subscription_table;


CREATE TABLE subscription_table (
    id integer PRIMARY KEY,
    name text NOT NULL,
    email text NOT NULL
    );